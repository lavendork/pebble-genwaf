
module.exports = [
  {
    "type": "heading",
    "defaultValue": "Generative Watchface customization"
  },

  {
    "type": "section",
    "items": [
      {
        "type": "input",
        "messageKey": "seed",
        "defaultValue": "1234",
        "label": "Seed for background generation",
      },
    ]
  },

  {
    "type": "submit",
    "defaultValue": "Save Settings"
  }
];

#include <pebble.h>
#include "settings.h"

static Window *s_window;
static Layer *s_window_layer;

static Layer *s_background_layer;
static TextLayer *s_time_layer;

static char background_seed[32];

// Called During the timeline quick view popup animation
static void app_unobstructed_change(AnimationProgress progress, void *context) {
  GRect new_bounds = layer_get_unobstructed_bounds(s_window_layer);;

  // Only square watches have quick view, so it's safe-ish to not check if round
  layer_set_frame(
    text_layer_get_layer(s_time_layer),
    GRect(0, new_bounds.size.h - 60, new_bounds.size.w, 49)
  );
}

// This is its own function so it can be called in window_load
static void update_time(struct tm *tick_time) {
  // From the tutorial at https://developer.rebble.io/developer.pebble.com/tutorials/watchface-tutorial/part1/index.html#telling-the-time
  // Write the current hours and minutes into a buffer
  static char s_buffer[8];
  strftime(s_buffer, sizeof(s_buffer), clock_is_24h_style() ?
                                          "%H:%M" : "%I:%M", tick_time);

  // Display this time on the TextLayer
  text_layer_set_text(s_time_layer, s_buffer);
}

// Called every minute to update the time
static void tick_handler(struct tm *tick_time, TimeUnits units_changed) {
  update_time(tick_time);
  layer_mark_dirty(s_background_layer); // Redraw the updated background
}

// Read settings from persistent storage
static void prv_load_settings() {
  // Read seed from persistent storage, if it exists
  persist_read_string(SEED_KEY, background_seed, sizeof(background_seed));

  //APP_LOG(APP_LOG_LEVEL_DEBUG, "loaded seed %s from persistent storage", background_seed);
}

// Handle the response from AppMessage
static void prv_inbox_received_handler(DictionaryIterator *iter, void *context) {
  Tuple *background_seed_t = dict_find(iter, MESSAGE_KEY_seed);
  if (background_seed_t) {
    strncpy(background_seed, background_seed_t->value->cstring, sizeof(background_seed));
    persist_write_string(SEED_KEY, background_seed_t->value->cstring);

    layer_mark_dirty(s_background_layer); // Redraw background with new seed

    //APP_LOG(APP_LOG_LEVEL_DEBUG, "recieved seed %s", background_seed);

  }
}

static void prv_window_load(Window *window) {
  s_window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_unobstructed_bounds(s_window_layer);

  s_background_layer = layer_create(bounds);
  layer_add_child(s_window_layer, s_background_layer);
  //TODO: write a custom update_proc for this

  // Create and set up text layer
  s_time_layer = text_layer_create(
    PBL_IF_ROUND_ELSE(GRect(0, 98, bounds.size.w, 49), GRect(0, bounds.size.h - 60, bounds.size.w, 49))
  );
  text_layer_set_font(s_time_layer, fonts_get_system_font(FONT_KEY_LECO_42_NUMBERS));
  text_layer_set_text_alignment(s_time_layer, GTextAlignmentCenter);
  layer_add_child(s_window_layer, text_layer_get_layer(s_time_layer));

  // Show time in text layer when it is initialized
  time_t temp = time(NULL);
  update_time(localtime(&temp));

  // Load seed for background
  prv_load_settings();

  // Subscribe to the obstructions event
  UnobstructedAreaHandlers handlers = {
    .change = app_unobstructed_change
  };
  unobstructed_area_service_subscribe(handlers, NULL);

  // Open AppMessage connection
  app_message_register_inbox_received(prv_inbox_received_handler);
  app_message_open(128, 32);
}

static void prv_window_unload(Window *window) {
  layer_destroy(s_background_layer);
  text_layer_destroy(s_time_layer);
}

static void prv_init(void) {
  s_window = window_create();
  window_set_window_handlers(s_window, (WindowHandlers) {
    .load = prv_window_load,
    .unload = prv_window_unload,
  });
  const bool animated = true;
  window_stack_push(s_window, animated);

  // Register for tick updates every minute
  tick_timer_service_subscribe(MINUTE_UNIT, tick_handler);
}

static void prv_deinit(void) {
  window_destroy(s_window);
}

int main(void) {
  prv_init();

  APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", s_window);

  app_event_loop();
  prv_deinit();
}

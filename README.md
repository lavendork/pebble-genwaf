**Pebble Generative Watchface**

The idea is to generate a pattern for the watch face that is based on the current system time, and a seed supplied by the end user. If two users supply the same seed, their watches will create the same background image.

**Todo:**
- [x] Show the time
- [x] Show the time in a pretty way
- [ ] Draw background
- [x] Allow changing the seed from the phone
